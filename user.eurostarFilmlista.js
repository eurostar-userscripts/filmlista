// ==UserScript==
// @description This script will add buttons to switch between dates on the Eurostar booking page.
// @name Eurostar.se - Filmlista
// @author Victor Evertsson Heijler
// @version 1.5
// @include http://eurostar.se/html/bokning.php*
// @include http://www.eurostar.se/html/bokning.php*
// @updateURL http://heijler.se/eurostar/user.eurostarFilmlista.js
// @run-at document-end
// @grant none
// ==/UserScript==

var days = document.querySelectorAll("select")[1];
var currentIndex = days[days.selectedIndex];
var prevIndex    = days[days.selectedIndex - 1];
var nextIndex    = days[days.selectedIndex + 1];

function renderVal(current, next, prev) {

	var wrapper      = document.createElement("div");
	var selectedDay  = document.createElement("p");
	var nextDay      = document.createElement("a");
	var prevDay      = document.createElement("a");
	var allDays      = document.createElement("a");
	nextDay.href     = "#";
	prevDay.href     = "#";
	allDays.href     = "#";

	wrapper.id       = "selector-wrapper";
	wrapper.style.cssText = "padding: 0 10px;border-radius: 5px;border: 1px solid #097196;width: 130px;height: 117px;font-size: 13px;position: absolute;top: 20px;left: 20px;background: #064B64;";
	selectedDay.style.cssText = "margin: 5px;padding: 10px;text-align: center;border-bottom: 1px solid #097196;";
	allDays.style.cssText = "text-align:center;display:block;margin:1em;";
	nextDay.style.cssText = "text-align: right;width: 65px;height: 29px;float: left;padding-top: 10px;border-top: 1px solid #097196;";
	prevDay.style.cssText = "text-align: left;float: left;width: 64px;height: 29px;border-right: 1px solid #097196;border-top: 1px solid #097196;padding-top: 10px;";
	document.body.appendChild(wrapper);
	selectedDay.innerHTML = current.value;
	allDays.innerHTML = "Alla dagar";
	wrapper.appendChild(selectedDay);
	wrapper.appendChild(allDays);
	if(prev !== false) {
		prevDay.innerHTML = "&laquo; Föreg.";
		wrapper.appendChild(prevDay); 
	}
	if(next !== false) { 
		nextDay.innerHTML = "Nästa &raquo;";
		wrapper.appendChild(nextDay);
	}

	function renderNextDay() {
		days.selectedIndex = days.selectedIndex + 1;
		document.search.submit();
	}

	function renderPrevDay() {
		days.selectedIndex = days.selectedIndex - 1;
		document.search.submit();
	}

	function renderAllDays() {
		fastSearch("alla");
	}
	nextDay.onclick = renderNextDay;
	prevDay.onclick = renderPrevDay;
	allDays.onclick = renderAllDays;
}

if (days.selectedIndex == 1 || days.selectedIndex == 0) {
	renderVal(currentIndex, nextIndex, false);

} else if (days.selectedIndex == days.length - 1) {
	renderVal(currentIndex, false, prevIndex);

} else {
	renderVal(currentIndex, nextIndex, prevIndex);
}

// Set background all black, looks a bit better
document.body.background = "";




// Permanent button for all days! fastSearch('alla');
// Tweak logic to grey out next and prev if no choice, instead of removing.
// Make it dragable, and set coords in localstorage or something
