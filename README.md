#![Filmlista](https://i.imgur.com/thihgdr.png)

***

Skapar en modul som gör det lättare att navigera mellan dagarna på eurostar.se med en "föregående" och "nästa" -knapp.

Detta skript använder selectedIndex för att välja rätt dag, och inbyggd funktion i eurostars sida för att skicka värdet till servern. Den använder också redan existerande funktion för att visa alla dagar.